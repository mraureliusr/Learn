% !TeX root = ../learn_linux_amr.tex
\appendix
\chapter{Version Control with Git}
\section{What is Version Control?}
When you create any document on a computer, especially source code and large documents, there are typically many revisions of the document before it is ready to be released. Whether you are working on the project alone or with others, it is useful to keep track of when changes were made and for what reason. This is what version control software does. Every time you are finished working on a particular section or feature, you take a "snapshot" of the project at that moment, called a \textit{commit}. These commits have messages attached to them explaining the changes made. As you go along and add more commits, you end up with a log of who changed what, and at what time. 

This is useful for keeping track of projects, but the real power of version control is being able to merge in changes from other people, as well as move backwards through the commit log if needed. You can revert to a previous commit if changes were committed that should not have been. This is almost like having a continuous backup as you work through the project, and by also have a remote server that you \textit{push} the commits to, you have a backup in two places.

\section{What is Git?}
Git is a very popular version control tool, also built by Linus Torvalds, the creator of Linux. He wrote the tool to help manage all the developers working on Linux; so it needed to be very fast and lightweight, and be able to handle huge files and many people working on the same project. Since its release, it has become one of the most popular version control systems in the world. Git, like almost all version control systems, keeps track of the \textit{changes} made to each file, and not a complete copy of the file itself. This makes the repository smaller, but we can still revert any file back to any point in time if needed.

\subsection{First Steps}
So, you've got a project you want to track. If you don't have one, just create a directory and create a text file or two. The very first thing we need to do is tell Git that we want this to be a Git repository. We do this with the very simple command:

\verb+git init+

This will create an empty Git repository in our current directory. Now we need to tell Git which files to track. You can pick and choose which files to track; it's not necessary to track every file in the project if you don't want to. However, for our example, let's add all the files in the repository:

\verb+git add *+

This will add every file and folder in the directory. This is where newcomers often stumble when learning Git for the first time; once you have \textit{added} a file to git, that file is now \textit{staged}. This means that if you were  Go ahead and make a change to the text file, and then try this command:

\verb+git status+

Let's take a look at an example I copied during the creation of this book. Don't be put off by the new terminology - it will all make sense after we go through them one by one.

\begin{verbatim}
On branch master
Changes to be committed:
(use "git reset HEAD <file>..." to unstage)

deleted:    learn_linux_amr.idx
deleted:    learn_linux_amr.ilg
deleted:    learn_linux_amr.ind

Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git checkout -- <file>..." to discard changes in working directory)

modified:   .gitignore
modified:   chapters/chapter1.tex
modified:   chapters/chapter2.tex
modified:   chapters/chapter3.tex
modified:   chapters/chapter4.tex
modified:   chapters/chapter5.tex
modified:   chapters/chapter6.tex
modified:   chapters/chapter7.tex
modified:   learn_linux_amr.dvi
modified:   learn_linux_amr.pdf
modified:   learn_linux_amr.synctex.gz
modified:   learn_linux_amr.tex

Untracked files:
(use "git add <file>..." to include in what will be committed)

chapters/appendixA.tex
layout.md
\end{verbatim}

The very first thing we see at the top is \verb+On branch master+. Git can have many different branches; each branch is a copy of the files that Git tracks. So you can make changes that head in two different directions and decide which you want to use later. We will go into branches in more detail later. Then, Git lists all the changes that would be committed, i.e., logged, if we were to make a commit right now. Anything listed under \verb+Changes to be committed:+ are changes that will be a part of the commit. The files listed under \verb+Changes not staged for commit:+ are files that were tracked by Git in the previous commit, have been changed, but we haven't yet told Git we want to add them to the next commit. And the last group, \verb+Untracked files:+, are files that we haven't told Git to watch at all. These will not be added to the next commit, and aren't tracked in any way.

There is some terminology I want to make clear, because it's often confusing. Even after you \textit{add} a file to a commit, it's not automatically added again after you commit. Adding a file makes Git track it, and adds it to the staging area. The staging area is the place where files sit before they get committed. So even if you add a file, it means after you commit you need to add it again. However, there are ways to make this much easier, which we will go over in the section on committing.

\section{Committing}
So now we have our files changed, they are staged (because we ran \texttt{git add} on them), and so we are ready to commit them. Every commit has a message associated with it, so that people looking at your repository can understand what changes were made. So simply run the command:

\verb+git commit+

By default, this will open up the default editor on your system (nano, unless you have changed it). Simply type an explanation of the changes you've made, then save the file and quit nano. Git will now make a commit. You can view the list of commits made by typing:

\verb+git log+

This command is very important when using Git. Looking at the history of your repository can often help you keep things organized; not to mention it's important to check that your commits were made properly. If we take another look at the status command, you'll notice that the files you staged and committed previously are now \textit{tracked} but not staged for commit. There is an easier way to commit all tracked files than adding them before every commit. If you add the \texttt{-a} flag to \texttt{git commit} it will automatically add any tracked file that has changed since the last commit. In most cases, this is what you will want, and this flag makes it much easier.