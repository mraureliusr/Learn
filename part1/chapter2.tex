% !TeX root = ../learn_linux_amr.tex
\chapter{Getting Started} \label{ch:Getting-Started}
\section{Virtualisation}
\subsection{What is Virtualisation?}
Virtualisation is the concept of running an OS inside another OS. We refer to the outer OS as the \textit{host}, or sometimes the \textit{master}, and the virtual OS as the \textit{guest}, or sometimes the \textit{slave}. The term often used to describe these virtual installations is \textit{virtual machine}, or more commonly just VM. 

The benefits of virtualisation in this course are many. It obviates the need for the student (that's you!) to go through the process of actually installing Ubuntu. I can distribute a pre-made disk image of Ubuntu that is known to work with the virtualisation software we will be using. And because the virtual OS is contained inside the virtualisation software, it's much less likely that you will be able to cause any damage to the host OS. Most of the students following this course will be using Windows or MacOS, and so making it very easy to start playing with Linux is essential to making this course accessible and simple to follow.
\section{VirtualBox}
\subsection{About VirtualBox}
There are many different virtualisation packages available. VMWare, VirtualBox and Xen are some of the most popular. We are using VirtualBox because it is free, very easy to setup and use, and has excellent support for a wide variety of guest OSes and host hardware. 
\subsection{Downloading VirtualBox}
Simply go to \url{https://virtualbox.org}. There will be a Download link in the left sidebar. From here, we need to download the Platform Package for your host OS. For example, if I was running Windows 10, I would click on Windows Hosts. Further down the page, there is another download link for a package called the VirtualBox Extension Pack. While this isn't strictly needed for our curriculum, it will make the experience better and add many options in case you want to use your virtual Ubuntu beyond the end of this course.
\subsection{Installing VirtualBox}
Once the files have finished downloading, start the installer first. It should be called something similar to \texttt{VirtualBox\--5.2.12-122591-Win.exe}, though the version number will be different for you. If you are on MacOS, it will end with \texttt{-OSX.dmg}. The installer should be fairly self-explanatory. When it asks if it's okay to install the networking drivers, which will reset all network interfaces and temporarily disconnect you from the internet, click Yes. In my experience, the reset happens so quickly that I don't notice my internet connection dropping at all. 

After the installer is finished, find and run the Extension Pack installer. It will have a name similar to \texttt{VirtualBox\-\_Extension\-\_Pack\-5.2.12.vbox-extpack}. Once VirtualBox is installed, double-clicking on this file should automatically open VirtualBox and ask you if you want to install the Extension Pack. Again, installation should be simple. If you have any problems with either of these installations, simply ask your instructor for assistance.
\subsection{Downloading the Ubuntu Image}
Now that VirtualBox is installed, it's time to get the pre-prepared copy of Ubuntu Budgie that I have created for this course. Please note that the download itself is about 3GB, and after it is imported into VirtualBox it will expand to about 6.6GB. Make sure you have at least 10GB free before starting this process. You can delete the \texttt{Ubuntu.ova} file after it has been imported to free up some space. It's hosted on my website, and can be downloaded at \url{http://learnlinux.ml}.

You have the option to download the file directly over HTTP, or by using the torrent file. For most people, the torrent file is the faster, safer option, but if you have a fast, stable connection, you can download it directly from the website. It may take a while to download on a slow connection.

\begin{boxedsec}{Note}
You should take the time to check the hashes of the downloaded image file. For more information on what hashes are, and instructions on to check them, see the website! This will make sure the file you downloaded did not get corrupted in transit.
\end{boxedsec}

\subsection{Importing the image into VirtualBox}
After the file is finished downloading, open VirtualBox, then click on File..., then Import Appliance... (or just hit Ctrl+I). Navigate to the \texttt{Ubuntu.ova} file, then click Import. This process may take a while, depending on the speed of your hard drive, how much RAM you have, and what processor you have. However, it shouldn't take more than 10-15 minutes.
\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{virtualbox}
	\caption{VirtualBox after the Ubuntu.ova file has been successfully imported}
	\label{fig:virtualbox}
\end{figure}
\subsection{Running Ubuntu for the first time}
Congratulations! You are now the proud owner of a brand new Ubuntu Budgie 18.04 LTS install. Before we click Launch and start the VM, it's important to consider a few things. This VM expects to be able to use at least one gigabyte of RAM. Depending on how much RAM your system has, this may have either a large performance impact or none at all. If you have more RAM available (eight gigabytes or more) you may consider increasing the amount of RAM given to the guest OS. You can do this by clicking on the name of the VM to highlight it, and then clicking on Settings. Under the System tab, you can specify how much memory the VM can reserve. Somewhere between 2048-8192MB is ideal, depending on how much RAM you have. If you have sixteen gigabytes or more, I would strongly recommend at least 4096MB. However, the preset value of 1024MB will be adequate to complete this course.

Another consideration is disk space. The VM has a "virtual" drive of 32GB. But, you might ask, how was the download only 3GB? This is one of the clever tricks of virtualisation: dynamically sized disks. The file that represents the hard drive will grow as more files are added to the system. However, the guest OS thinks it has 32GB, and as it writes to the free space on its drive, the file on your hard disk will grow. This is very useful as it limits the amount of storage needed, at the cost of a bit of performance.

The last and most critical consideration is virtualisation technology. Using virtual machines has become so commonplace that Intel and AMD added special instructions to their CPUs. These instructions speed up VMs and allow them direct access to RAM and peripherals, significantly increasing the performance of the VM. This takes some of the task of virtualising the system out of software and into hardware, which is much faster. However, in most computers, these extensions are disabled by default because viruses could potentially take advantage of them to cause damage. This choice is left up to you, but if you are able to and are comfortable doing so, I \textit{strongly} recommend rebooting your computer, entering your BIOS, and enabling any virtualisation options you can find. If you are having trouble, look for \texttt{Intel VT} or \texttt{AMD-V}. You may also see the term \texttt{IOMMU}; if so, enable it as well. If you are unsure about how to do this, a quick Google search for the terms "enable processor virtualisation" returns myriad web pages with detailed instructions. If you are still unsure, ask your instructor for assistance.

With all these considerations in mind, go ahead and start the virtual machine for the first time, by highlighting the name of the VM (Ubuntu) and then clicking on the Start button above. As discussed previously, the boot process may take a while depending on your hardware configuration. On my current laptop, which is almost three years old, it takes about 15-20 seconds to get to the desktop. Newer machines will be much faster than this. Once you are at the desktop, feel free to poke around.
\subsection{Familiarize yourself with Ubuntu Budgie}
If you are used to Mac, the Budgie environment won't seem completely unfamiliar. However, if you are a Windows user, the first thing you will notice is that the system bar (called the taskbar in Windows) is at the top! Once you get used to this, you may find you like it better. The applications menu is the button in the top left, and the system tray in the top right has things like laptop battery charge, Wi-Fi and network connections, and more - just like you are used to in both Windows and Mac. Open the Chromium browser and try navigating to a website, such as \url{https://en.wikipedia.org}, to ensure that networking is properly set up. If possible, I highly recommend downloading this PDF inside the VM, and then going fullscreen to have a more immersive experience.

\begin{boxedsec}{Important$!$}
	The password for the \texttt{ubuntu} account is \textbf{learnlinux}. I made it easy to remember because I didn't want students having to refer back to this page. However, if you do anything with the VM beyond this course, you should change the password using the \texttt{passwd} command in subsection \ref{subsec:Changing-passwords} of chapter \ref{ch:Users-Groups}.
\end{boxedsec}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{ubuntu_fresh}
	\caption{This is what the Ubuntu Budgie desktop environment looks like after the first boot}
	\label{fig:ubuntu_fresh}
\end{figure}