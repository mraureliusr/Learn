% !TeX root = ../learn_linux_amr.tex
\chapter{Welcome to Linux!} \label{ch:Welcome-to-Linux}
\section{The Basics}
\subsection{The Desktop}
When you first boot into Ubuntu Budgie, it will automatically log you in and bring you to the desktop. If you spent some time poking around after your first successful boot, you've no doubt found the web browser and other accessories. Most users find the Budgie interface very intuitive and easy-to-use. If you have trouble finding something, just ask your instructor, or check online. 

\begin{boxedsec}{Tip}
Ubuntu has an extremely good forum which is run by StackExchange. Almost every Ubuntu related question has been asked and answered here at some point. You can find it at \url{https://askubuntu.com/}. I have added this website as a bookmark in Chromium Browser. However, I must caution you: never, \textbf{ever} copy and paste a command you don't understand into the terminal. The best case is it doesn't work, and no damage is done. The worst case is you could allow an attacker access to your system. While that may not matter in this VM, it's a good habit to get into. You wouldn't try to use a chainsaw without understanding how it works, because you could hurt yourself. So make sure you understand what commands do before running them, or you might chop off some digital fingers!
\end{boxedsec}


\begin{boxedpic}
	\centering
	\includegraphics[width=\textwidth]{tilix}
	\caption{The Tilix graphical terminal program. This terminal is very flexible, offering a tabbed interface and extensive customisation, and is the default in Ubuntu Budgie.}
	\label{fig:tilix}
\end{boxedpic}


\subsection{What is a command?}
A command is any instruction that you give the operating system, typically as text. Almost all commands follow the same syntax pattern:\\

\verb+command flags input/output+ \\

So it will be the name of the command, followed by any optional flags (which we will explain in a bit), followed sometimes by the name of the file, directory, or other object you are executing the command on. Anything that comes after the name of a command is called an \textit{argument}. \index{argument}

\subsection{The Terminal}
Here is the major stumbling block for most new Linux users: the horror of facing the terminal! Let me tell you right now -- you \textit{can} master it. It's not nearly as difficult as many movies and TV shows make it seem. The terminal, or command line, is a text-based interface between you and the operating system. You type in text commands, and the operating system will show you the results of those commands. Common tasks done this way include creating directories, moving files, and basic text editing. We will start slowly, and work our way up to mastery, one command at a time. If you're as old as I am, you might remember using DOS when you were much younger. If you did, you'll recall that you really only needed to know a few commands to get everyday things done. The same is true with the terminal in Linux.
\subsubsection{What is BASH?}
A quick aside here. Earlier I spoke about how Linux is simply a kernel, and cannot do much of anything on its own. This is where the \textit{shell} comes in. Shell is just a fancy way of referring to the command line. Unlike DOS, where the command line was built into the operating system itself, with Linux you have the choice of using many different shells, if desired. The universal shell across almost all modern Linux distributions is BASH (short for the \textbf{B}ourne \textbf{A}gain \textbf{SH}ell). The practical differences between shells are way beyond the scope of this course, however I believe it to be an essential piece of knowledge when learning Linux. Like many things in this OS, there are usually many different ways to accomplish the same task. The reason the BASH shell is almost universally the default shell is because it implements all the standard features that users expect, it's very lightweight and fast, and standardizing all distributions around one shell is good practice. That way, no matter which Linux system you find yourself using, all the features you've learned will work.

\subsection{Learning Your First Commands} \label{subsec:Learning-Your-First-Commands}
Double-click on the Tilix icon on the desktop, if you haven't already opened it. You will be greeted with a prompt:
\verb+ubuntu@ubuntu-VirtualBox:~$+ \\
We will go into more depth about what the prompt means later, but for now I want to draw your attention to two parts of it. The first \texttt{ubuntu} in the prompt is showing your username. If you were logged in as a different user, it would display that username instead. Also, the tilde (this thing \large\url{~}\normalsize) is representing the current directory we are in. The tilde is a shortcut that represents our user's home directory. Why don't we take a look at what's inside our home directory?

\subsubsection{ls: List Directory}
In your youthful days of yore, you may remember using the command \texttt{dir} to display the current directory. Well, in Linux, the command is \texttt{ls}. Go ahead and type it now, and press Return (also known as Enter). You should see output like this:

\begin{verbatim}
ubuntu@ubuntu-VirtualBox:~$ ls
Desktop    Downloads  Music     Public     Videos
Documents  LL.txt     Pictures  Templates
\end{verbatim}

This is very similar to the user folder on Windows. You'll see we have a Documents directory, as well as Downloads, Music, Pictures, etc. This is where you can store any files that belong to you. There's also a file named \texttt{LL.txt} which we will use later.

Like almost all commands we will encounter, \texttt{ls} can take \textit{flags}, which are modifiers that are typed in after the name of the command to change the way it behaves. A common flag for \texttt{ls} is \texttt{-l} (that's a hyphen and a lowercase L). This will list the directory in a more technical format, showing you much more information about each file. Go ahead and try it now by typing \texttt{ls -l} and hitting Return.

\subsubsection{cd: Change Directory}
Now that we can see directories, how do we move through them? Well, one of the commands you will use the most is \texttt{cd}. Its syntax is very simple - just type the command followed by the directory you would like to move into. For example, we can move into Documents by typing \texttt{cd Documents}. Once you have done this, type \texttt{ls} to see what is in there. Because we haven't created any documents yet, the listing will be empty. 

Notice how the prompt has changed? It now says:
\\
\verb+ubuntu@ubuntu-VirtualBox:~/Documents+
\\
to reflect the fact that we are now in the Documents folder, underneath the home folder. Just like how the tilde represents the home directory, there are two other shortcuts for representing directories. A single dot \texttt{.} represents the current directory. Two dots \texttt{..} represents the parent directory, i.e. the directory directly above the current directory. In Windows, you may remember the Up button. Typing the command \texttt{cd ..} has the same effect. Go ahead and run that command now to return to the home directory.

\subsubsection{pwd: Print Working Directory}
This command is very simple, and can be helpful if you are many levels deep in a directory structure and are confused about where you are. Most shell prompts already print the current directory, but \texttt{pwd} will print the entire path, replacing the tilde with the full path of the home directory. Go ahead and type \texttt{pwd} now. You'll notice that the home directory is actually /home/ubuntu/. This is what the tilde \textit{expands} to in directory names.

\subsubsection{mkdir: Make Directory}
Now that we can move through and see directories, the next logical step is to be able to make them as well! The syntax for this command is very simple. Just type the command followed by the directory you want to create. For example:
\\
\verb+mkdir test+ 
\\
You can also create a directory inside another directory. After typing the last command, try this one: 
\\
\verb+mkdir test/test2+ 
\\
Then if you type \texttt{ls test} you can see that you created test2 without having to be inside \texttt{test}. This can be very useful!

\subsubsection{rmdir: Remove Empty Directory}
You don't see this command used that often, because as the title says it can only be used to remove directories that are already empty. However, this can be much safer than blindly deleting a directory which may still have files in it! The syntax is the same as \texttt{mkdir}. Just the command followed by the name of the directory you want to remove. First, we need to remove the directory inside \texttt{test}:
\\
\verb+rmdir test/test2/+
\\
The trailing slash isn't strictly necessary, but I try to use it to lessen confusion. Now we remove \texttt{test} itself:
\\
\verb+rmdir test/+
\\
If you want to be very specific about the directory you want to remove, add \texttt{./} at the beginning, or type the full path name. Try creating and then removing a directory. Remember to type \texttt{ls} after each command to see what changes you've made.

\subsubsection{cat: Concatenate}
The name of this command is slightly confusing at first. The function for which it is named is the ability to attach two text files (or any type of file, technically) and print them to the screen, or be redirected into a new file. There is a file I mentioned earlier, \texttt{LL.txt}. Let's see what is inside that file. Type the command:
\\
\verb+cat LL.txt+
\\
and press Return. You should see the contents of that file printed to the terminal. This is a super handy way to quickly see the contents of a file without having to open a text editor.

\subsubsection{less: Paginate}
A related cousin of \texttt{cat} is \texttt{less}. The difference is instead of printing all the output to the screen at once, it will give it to you a page at a time. You can scroll up and down with the arrow keys, or you can jump ahead a page with the spacebar. Give it a try with:
\\
\verb+less LL.txt+
\\
\subsubsection{cp: Copy}
This command is very flexible and has a lot of options. However, in its most basic form, it just copies a file from one place to another. The syntax is:
\\
\verb+cp source destination+
\\
Try copying the file we just viewed with \texttt{cat} to a new file in the same directory. Name it whatever you wish! Make sure to type \texttt{ls} afterwards to see what changes were made!

\subsubsection{mv: Move or Rename}
A very close cousin of \texttt{cp} is \texttt{mv}. This takes the same syntax, except it actually \textit{moves} the file. If you don't specify a new location, it just renames the file. Additionally, you can do both operations with one command; you can rename a file while moving it to a new location. This can be very handy! Try moving the file you created with \texttt{cp} in the last section into the \texttt{/test} directory. Remember to start the destination with a dot! If you just type \texttt{mv LL.txt /test/LL2.txt} it will think that the test directory is in the \textit{root} directory, which is not the case. Re-read the section on \texttt{cd} if you need a refresher on what the dots mean.

\subsubsection{rm: Remove}
This command is extremely powerful, and can be very dangerous if you don't pay attention to what you are typing. \texttt{rm} is used to remove both files and non-empty directories. The classic example which shows how much control the user has in Linux is the command \texttt{rm -rf /}. \textbf{Do not try this command!} That command is saying "Delete the entire operating system, yes I'm absolutely sure, don't ask me if I am sure." So you can cause a lot of damage with \texttt{rm}. This command often does take flags, so we will take a look at them as well. The syntax is as follows:

\texttt{rm \textit{file}}

If we want to remove a non-empty directory, we have to add the \texttt{-r} flag. So it ends up looking like this:

\texttt{rm -r \textit{directory}}

Depending on the implementation of \texttt{rm}, this may or may not prompt you if you are sure you want to remove a non-empty directory. Adding the \texttt{-f} flag as we saw before will tell \texttt{rm} to never ask you if you are sure; just execute the command. So be very careful and always double-check your commands before you hit Return!

\subsection{Shell Features} \label{subsec:Shell-Features}
The BASH shell has two very powerful built-in features which, when UNIX was first released in the 70's, made it revolutionary and very popular. These are called \textit{operators} \index{operators} and there are two types: control operators and redirection operators. Don't worry too much about this terminology right now. We will look at examples of both and the meanings of these terms will become clear. The first feature we will look at is called \texttt{piping}. 

\subsubsection{Piping} \index{piping}
The pipe symbol (|), variously called vertical bar or, occasionally, vertical slash, is a control operator. It \textit{pipes} the output of one command into the input of another command. For example, the command \texttt{sort} takes any text as its input and writes it to the screen in alphabetical order by default. We can use \texttt{cat} to display a file, but what if we want to sort it as well? You may have noticed another file in your home directory called \texttt{LLphonebook.txt}. This is a made-up listing of names and phone numbers that we are going to sort. This is very easy with pipes!

We can \textit{pipe} the output of \texttt{cat} into the input of \texttt{sort} like so:

\verb+cat LLphonebook.txt | sort+

The output is the list sorted alphabetically by their first name. But wait! There's a duplicate! Now, in this case, because there is only one duplicate, we could just remove it by hand. But imagine you had a file with many thousands of listings like this. How would you get rid of any duplicates? In our case, the easiest way is just to extend the pipe. We will add another program, \texttt{uniq}, which only returns lines from a file if they are unique (as the name suggests!). Let's give it a try:

\verb+cat LLphonebook.txt | sort | uniq+

Excellent! Now our list is sorted and any duplicates have been removed. You can see how this could save an immense amount of time if you had to sort many files. This is the power of the shell, and why it is so popular for doing tasks like these! What if we want to store this as a new file? This is where redirection comes in.

\subsubsection{Redirection Operators} \index{redirection}
Redirection is another feature that changed the way people used operating systems when UNIX was released. As a quick aside, there are two terms to describe input from our keyboard and output to our terminal -- \texttt{stdin} and \texttt{stdout} respectively. You don't need to remember these now, but they will help describe what redirection is doing.

Many commands can take input either as a filename, or they can take it from \texttt{stdin}. However, manually typing in all the input to a command would take forever. This is where redirection comes in. We can send input to a command from a file using the input redirection operator \texttt{$<$}, and we can take the output of a command and send it to a file using either of the output redirection operators \texttt{$>$} or \texttt{$>>$}. \texttt{$>$} will overwrite the file we specify with the output, whereas \texttt{$>>$} will append it. Because \texttt{$>$} can overwrite a file, make sure you are choosing the correct operator!

Let's take a look at \texttt{cat} as an example. We have been passing filenames to cat as an argument. We could also pass them using \texttt{stdin} redirection, like so:

\verb+cat < LL.txt+

Of course, this is a more obtuse way to do things. But for some commands, this is the only way to pass a file as input. For example, we could remove \texttt{cat} from our pipe example above, and simply do this:

\verb+sort < LLphonebook.txt | uniq+

At the end of the last section we wanted to store this output as a new file. Let's store it to \texttt{LLsorted.txt}:

\verb+sort < LLphonebook.txt | uniq > LLsorted.txt+

Now, take a look at the contents of \texttt{LLsorted.txt}. Pretty cool, right?

\begin{boxedsec}{Note}
These features of the shell are borne from one of the fundamental concepts of UNIX: instead of creating new programs to do simple functions, you can chain together multiple small (or large!) programs to achieve the same effect in much less time. While these concepts are still used today, back in the 1970s this idea was revolutionary. Prior to this, any time you wanted to, for example, sort the contents of a file and then store it, you had to write a new program by hand or find someone else who had already written such a program and get a copy. 
\end{boxedsec}

\subsubsection{Running multiple commands}
There is a subset of control operators called \texttt{logical} operators. These use simple logic to determine whether to continue running commands in a chain. The only logical operator we are going to take a look at now is the \textit{and} operator,~\texttt{\&\&}. This will run the second command only if the first command completed successfully. That is, you will only get the results you expect if both the first \textit{and} the second commands complete without errors. In most cases, however, we simply use this operator to run multiple commands in a row without having to wait for the previous command to finish. This can be very useful if you are running programs which take a long time to execute, for example compiling large amounts of code. You can simply type all the commands you want to run, and then walk away from the computer and have a coffee.

Let's say we wanted to view the file we sorted the output into earlier to make sure it did everything correctly. We can do this using the \texttt{\&\&} operator. It's really quite intuitive:

\verb+sort < LLphonebook.txt | uniq > LLsorted.txt && cat LLsorted.txt+

This does exactly the same thing as before; it takes the file \texttt{LLphonebook.txt}, sorts it alphabetically, and passes it to \texttt{uniq} to get rid of any duplicated lines. \texttt{uniq} then redirects its output to the file \texttt{LLsorted.txt} instead of printing it to the screen. Assuming all this happened successfully, the second part will run: using \texttt{cat} to display the contents of \texttt{LLsorted.txt} to the screen.

\subsection{Getting Command Help: manual pages} \index{man}
Because there are so many commands and even the most advanced Linux administrators forget how they work, the \textit{manual pages} or \textit{man pages} were created. They are invoked by using the command \texttt{man} followed by the name of the command you are curious about. For example, to get help with the \texttt{cat} command, we would type:

\verb+man cat+

This opens up the formatted manual for that command in \texttt{less}. Manual pages are extremely thorough and can be very long-winded and difficult to parse at a glance. However, they are the authoratative reference for every command, and so they are extremely valuable for both new and old users alike. 

Phew! That is a lot to take in at once. Remember, you can refer back to this section whenever we come across these operators later on in the book.

\begin{boxedtoc}{Chapter Exercises}
	\begin{enumerate} 
		\item Create a directory and move the file \texttt{LL.txt} inside it.
		\item Display the contents of \texttt{LL.txt} without using an editor.
		\item Move into the directory you created, rename the text file to whatever you like, and then move back into the parent directory.
		\item Make a copy of the renamed file called \texttt{LL.txt}.
		\item Delete the renamed file (not \texttt{LL.txt}), without moving into the directory it is in.
		\item Move \texttt{LL.txt} back into the parent directory.
		\item Remove the directory you created.
		\item Sort the contents of \texttt{LLphonebook.txt} and print them to the screen.
		\item Sort the contents of \texttt{LLphonebook.txt}, remove any duplicate lines, and print it to the screen.
		\item Do the above, but this time redirect it to a new file.
		\item Print the file to the screen, and then remove it, using the and operator to accomplish both tasks in order.
	\end{enumerate}
\end{boxedtoc}