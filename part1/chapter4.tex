% !TeX root = ../learn_linux_amr.tex
\chapter{Editors} \label{ch:Editors}
So now we can see, create, remove and rename files and directories. Now we can start looking at different text editors available in Ubuntu. There are many, many different editors, and which one is better is left up to the reader. There have been endless internet battles about which editor is the best and why. Each editor has its own pros and cons, and you will naturally find the one you enjoy using the most. Editors are very different from word processors you might be used to like Microsoft Office or LibreOffice. They do not have the ability to change font sizes and styles, they usually do not have WYSIWYG\footnote{What You See Is What You Get}-style editing and preview, and they are almost always used for writing code or editing configuration files. Every distribution of Linux will come with a very basic and fast text editor. And in most cases, it is the first entry in our list -- \texttt{nano}. However, for historial reasons, you will almost \textit{always} be able to use \texttt{vi} as well, even if \texttt{nano} isn't present.

\section{Common Editors}
\subsection{Terminal-based Editors}
\subsubsection{nano: Tiny Text Editor}
\texttt{nano} is a very lightweight and simple text editor. For the vast majority of simple editing tasks, using nano is the fastest and easiest way to get your job done. You can either type \texttt{nano} on its own, or you can specify a file to edit by specifying it after the command. Let's try editing the file we saw earlier:

\texttt{nano LL.txt}

You will be greeted by a very simple screen, with the file at the top and a few options along the bottom. The caret character (\textasciicircum) is universally used in Linux and Unix-like systems to refer to the Ctrl key. So \textasciicircum K means hold down Ctrl, and press K. Try using the arrow keys on your keyboard to move to the bottom of the file, and then add another line of text. In \texttt{nano}, saving a file is called 'writing out' the file. As you can see at the bottom of the screen, the command for Write Out is \textasciicircum O. So save the file, and then to exit, the command is \textasciicircum X. Exit, and use \texttt{cat} to print the file to the screen to show the changes you made to the file.

You may have noticed how extremely fast \texttt{nano} is. It opens instantaneously, and saves files and exits just as quickly. This is why it has been almost universally adopted as the default editor in most distributions. In Linux and Unix-like systems, there is a command called \texttt{editor} which will open whatever the default text editor on that system is, and it will almost always be \texttt{nano} or its very close cousin \texttt{pico}.

\subsubsection{Emacs: Editor Macros}
While very few people know what Emacs actually stands for, it is one of the oldest and most widely used text editors on all *nix systems. With Emacs recently passing its 40\textsuperscript{th} birthday, it was written long before the advent of Linux. As a result, its age has granted it eminence within the *nix community, and many programmers fiercely defend it as the greatest editor ever written.

Emacs is actually both a terminal-based and GUI editor, but in my experience, I've seen more people use it in terminal mode than in GUI mode. The true power in using Emacs comes from mastering its keyboard shortcuts, thus negating the usefulness of a GUI. Typing keys on a keyboard is inherently faster than moving a mouse and then clicking it. However, Emacs has a fairly steep learning curve, and because of this most beginners shy away from it. I found the built-in tutorial to be a good place to start, but you need to be prepared to let go of how you normally edit text documents or code.

The other powerful feature of Emacs is its extensibility. Advanced users of Emacs almost treat it as if it were itself an operating system. They send and receive email, surf the web, and of course write and compile code. This last feature is what got me hooked on Emacs. Being able to write, compile and debug code without having to touch the mouse sped up my development cycle quite a bit. Later in Part x we will see a bit of this functionality when we learn the basics of writing code in Linux.

\subsubsection{Vim: Vi Improved}
I decided to group both \texttt{vi} and \texttt{vim} together in the same section because they are directly related to each other. Vi and Vim (I will just say Vim from now on) fall somewhere between nano and Emacs in terms of functionality. It uses a very different style of interaction from Emacs or nano. You really do need a cheat sheet in order to get used to Vim, as none of the commands are shown on screen.

The two important things to know with Vim, if you are ever in a position where you must use it (or if you want to give it a try) are these:
\begin{enumerate}
	\item You must press \texttt{I} to get into "Insert mode" before you can make changes to the document. This trips up almost everyone the first time they use Vim.
	\item All commands are entered by first typing a colon (:). For example, to save and exit you would type \texttt{:wq} and then press Return. You will notice that the cursor moves to the command box at the bottom of the screen.
\end{enumerate}

\begin{boxedsec}{Tip}
	There is a recent fork of Vim called Neovim that is gaining in popularity very quickly. On the surface, it feels and acts similarly to Vim, but there are many differences which make users prefer one over the other. Try them both out to see which one you like! Note that the command to launch Neovim is actually \texttt{nvim} and not \texttt{neovim} as you might assume.
\end{boxedsec}

\subsection{GUI Editors}
Just as with terminal-based editors, there are a huge number of GUI editors available. We will quickly look at the two most popular ones. 
%note: This section should be expanded with more GUI editors, but I really haven't used any
%if you are reading this and have suggestions, *please* send them to me

\subsubsection{Gedit}
Gedit is the default GUI text editor in the GNOME Desktop (which Budgie is based on). Gedit is pre-installed on your copy of Ubuntu. It is very simple but also quite powerful, with code syntax highlighting for a huge number of languages and a plugin system that makes it very flexible. It is widely used by many as the default GUI editor of choice (including this author!) and in most distributions it is set as the default for opening text files.

Feel free to take a look in the Preferences menu, which is accessed through the three dots in the upper right hand corner. You can change the colour theme to one of the presets if you like some flair. 
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{gedit}
	\caption[Gedit]{The source code of this book open in Gedit! Note how simple the user interface is.}
	\label{fig:gedit}
\end{figure}

\subsubsection{Kate}
Kate is another popular editor. It straddles the border between text editor and word processor. It is the default text editor for KDE Desktop, which we are not using. Kate is not pre-installed because installing it would require a lot of disk space for all the KDE files needed to run it.

\subsubsection{Atom}
Atom is an editor that is similar to Gedit in many ways, except it is designed to feel more modern. A tabbed interface makes juggling many files easy, similar to Gedit. Some may prefer Gedit's uncluttered interface, but Atom offers more features with a similar look and feel. It has a built-in package ecosystem for extending and adding features. It has Github integration out of the box. Being hackable is one of the design guidelines of Atom, so you can change almost anything about Atom with some time and effort.

I started using Atom more recently because of its ease-of-use and features. Having Git integration in what is otherwise a fairly simple text editor is extremely useful for small code projects. 
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{atom}
	\caption{Atom's welcome screen showcases the many features that it offers!}
	\label{fig:atom}
\end{figure}

\subsubsection{Notepadqq}
Notepadqq is an attempt to re-create the extremely popular Windows text editor Notepad++ in Linux. If you are a fan of Notepad++ you will find yourself right at home. While it's not an exact port of Notepad++, it has a virtually identical look and feel. 

\subsection{Integrated Development Environments (IDEs)}
IDEs are powerful editors coupled with development tools like code syntax checking, compilers, debuggers, documentation generation tools, and much more. There are an enormous variety of IDEs for *nix systems. A few have gained immense popularity due to community support, extensibility, and quick bug fixes. We will go into much more depth on this topic in Part x, but for now we will take a quick look at some of the most popular IDEs. If you are already a programmer, feel free to install these after reading Chapter~\ref{ch:Installing-Software}.

\subsubsection{Eclipse}
Quite possibly the most successful open-source IDE ever made, Eclipse was originally targeted at Java developers, but has since been extended to support many other languages, including C, C++, JavaScript, PHP and more. It has also been ported to support specific applications, such as microcontroller code development. It has a steeper learning curve than many other IDEs because of its extreme flexibility and large feature set. It can be set up to target many different types of application development. You can check out more at \url{https://www.eclipse.org/}.

\subsubsection{Geany}
I haven't used Geany that much, but it has an excellent reputation for being fast, lightweight and flexible. It supports C, Java, Perl, Python, PHP and more. It was designed specifically to be small in terms of file size, and also to have few requirements for running it. Like most IDEs it supports syntax highlighting and auto-completion. You can learn more at \url{https://www.geany.org.}.

\subsubsection{Code::Blocks}
Code::Blocks (typically written as just CodeBlocks) is a lightweight and cross-platform IDE with many features. It falls in between Geany and Eclipse in terms of features, and in this author's humble opinion, it hits the sweet spot between functionality and learning curve. We will make use of CodeBlocks later in this book, so feel free to open it and take a look if you are curious (it's pre-installed for you!). It can support many languages, but it is targeted at C/C++ developers. Like most of the IDEs in this list, it can be reconfigured to support almost any language.

\subsubsection{Visual Studio Code}
With Microsoft's recent move toward embracing open-source software, they have made a lightweight version of their professional IDE Visual Studio available under open-source licenses, calling it Visual Studio Code. It has quickly gained a large following, and many developers (especially web developers) have embraced it. There is a fork called VSCodium that has removed all of the code which sends usage statistics and other info back to Microsoft in order to increase the privacy of users. The usage of one over the other is a hotly contested debate and you'll have to decide for yourself which version, if any, to use.

\begin{boxedtoc}{Chapter Exercises}
	\begin{enumerate}
		\item Which editor is the default on most Linux systems?
		\item What does the character \textasciicircum{} mean?
		\item Name a terminal editor and a graphical editor.
		\item What is the difference between an editor and a word processor?
		\item What makes an IDE different from an editor?
		\item Which editor is the default GUI editor on GNOME?
		\item Name a feature that most IDEs have.
	\end{enumerate}
\end{boxedtoc}
