% !TeX root = ../learn_linux_amr.tex

\chapter{File System and Permissions} \label{ch:Files-and-Permissions}
\section{File System Hierarchy}
\subsection{Linux Standard Filesystem Hierarchy}
We've used \texttt{ls} to look at directory listings. We also spoke about the various symbols used to represent long path names (such as \url{~} being short for /home/ubuntu); now we are going to look at the file system in more depth. The directories we are going to look at are defined in a set of standards maintained by the Linux Foundation. These standards describe what a *nix system needs to have in order to be compliant with these standards. We won't worry about what that means, but it guarantees that you will always see the same folder layout in the root directory. These are defined in a document called the Standard Filesystem Hierarchy or FHS.\footnote{You can view the entire document at \url{https://wiki.linuxfoundation.org/lsb/fhs}}

\begin{boxedsec}{Note}
	One of the concepts that new users of Linux have difficulty understanding is that \textit{everything is a file}. Your keyboard, hard drives, running processes, absolutely everything is represented as a file. This may seem very confusing at first, and thankfully  we don't actually need to fully understand this concept! Just keep this in mind when reading the Standard Hierarchy section below.
\end{boxedsec}

What is the root directory? Simply \texttt{/}. That is the very top directory. This is not quite equivalent to C:$\backslash$ in Windows, but for now you can think of it that way. If you go ahead and run the command \texttt{ls /} you will see the following output:

\begin{verbatim}
bin    dev   initrd.img      lib64       mnt   root  snap      sys  var
boot   etc   initrd.img.old  lost+found  opt   run   srv       tmp  vmlinuz
cdrom  home  lib             media       proc  sbin  swapfile  usr
\end{verbatim}

Not all of these are directories, but we will quickly cover the important ones. There is no need to memorise these, but you may want to refer to this page later on if you forget where something should go.

\subsection{Standard Hierarchy}
\begin{tabular}{m{0.1\textwidth}m{0.8\textwidth}}
	/ & Root directory of the entire filesystem \\
	/bin & Binary files, i.e., executables such as \texttt{ls} and \texttt{cat} \\
	/boot & Bootloader files. This is where the files used to start Ubuntu live \\
	/dev & Device files, such as /dev/sda for the first hard drive \\
	/etc & System-wide configuration files \\
	/home & Where all the users' home directories are located \\
	/lib /lib64 & Libraries that are needed by the applications in /bin and /sbin. They are separated into folders specific to 32-bit and 64-bit systems to avoid confusion. \\
	/media & Mount points for removable media such as CD-ROMs or SD cards \\
	/mnt & Temporarily mounted file systems \\
	/opt & Optional software packages (typically used for things like games) \\
	/proc & File system which has files representing every running process \\
	/root & The root user is special, and gets its own home directory in the root directory \\
	/sbin & System binary files needed to run the OS \\
	/srv & Folder used for web servers, FTP servers and the like (legacy use only) \\
	/sys & Information about the system, including drivers and some aspects of the kernel \\
	/tmp & Temporary files, usually cleared on reboot (also /var/tmp) \\
	/usr & Contains userspace binaries and other read-only data. The structure of /usr is very similar to the structure of / \\
	/var & Variable files; these are expected to change often. Things like logs, cached files, databases, etc.
\end{tabular}

\section{File Permissions and Ownership}
\subsection{What are Permissions?}
Remember that \texttt{-l} flag that we used with \texttt{ls}? It gave us a much more detailed look at the current directory. Let's take a look at the output of \texttt{ls -l} in our home directory:

\begin{verbatim}
  ubuntu@ubuntu-VirtualBox:~$ ls -l
  total 36
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  9 16:33 Desktop
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:59 Documents
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:59 Downloads
  -rw-rw-r-- 1 ubuntu ubuntu  132 Jun  9 10:14 LL.txt
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:59 Music
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:59 Pictures
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:59 Public
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:52 Templates
  drwxr-xr-x 2 ubuntu ubuntu 4096 Jun  2 23:59 Videos
\end{verbatim}

Some of this output is easy to figure out. The date is the last time the file or directory was modified. To the left of that is the size of the file. You'll notice that directories are always of size 4096. The next two columns to the left are the owner and group of that file. Every file has an owner. The user who owns a file can change its permissions and change what group it is in. Groups are simply that -- groups that users can be added to. Every user has their own group, in which they are the only member. However, in larger systems where many users might need to access the same files, the administrator can create a group and add all the necessary users to that group. This makes keeping track of permissions much easier, and helps improve the security of the system.

We will skip over the single digit number, and get to the leftmost column. This string of gibberish is actually telling us who is allowed to read, write and execute each file.

There are ten positions in this sequence. Each one can either be a particular letter or a hyphen to indicate it is disabled. The very first position tells us if it is a directory or not. Notice how \texttt{LL.txt} is the only one that has a hyphen in place of the d?

The next nine positions are split into three groups of three letters. The first three are the file permissions for the \textit{owner} of this file/directory, the second three are the permissions for the \textit{group} the file belongs to, and the last three are the permissions for everyone else on the system. Let's take a closer look at \texttt{LL.txt} as an example.

\verb+-rw-rw-r-- 1 ubuntu ubuntu  132 Jun  9 10:14 LL.txt+

The first position is a hyphen, so this is a file and not a directory. The next three are \texttt{rw-}. This means the owner of the file (\texttt{ubuntu}) can read and write this file, but not execute it. This makes sense, as it is a text file. The second group is the same as the first, meaning the group this file belongs to (also called \texttt{ubuntu}) can also read and write the file. However, if you are not the user \texttt{ubuntu} or in the group \texttt{ubuntu}, you can only read the file (\texttt{r--}). Check out Figure \ref{fig:permissions} for a visual representation. That wasn't so complicated, was it? The permissions system in Linux is very useful and one of the reasons it is so widely used as a server operating system.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{permissions}
	\caption{How the permissions string is broken down}
	\label{fig:permissions}
\end{figure}

\subsection{Modifying File Permissions}
Knowing how to modify file permissions is an extremely important piece of knowledge for any Linux user. There will almost certainly come a time when you need to change the permissions on a file, and if you don't understand how to do this beforehand, you will end up tearing your hair out trying to understand what you are doing.

\subsubsection{A Crash Course in Octal}
If you have any experience with binary, octal, or hexadecimal you will find this section to be a breeze. However, most new users to Linux have not used any of these different bases, and so we'll have a quick lesson on octal.

Permissions are usually given in octal form when entered in a command. Octal is the name for base 8, that is, instead of having 10 digits (0-9), we only have 8 (0-7). Thankfully for our purposes we never have to think about anything beyond single-digit octal numbers. Binary is base 2, so we only have two numbers: 0 and 1. To convert between the two, we simply have to add up the value of each digit in the binary number. Let's take a look at what that looks like.


Think of each position in the permissions string as being a binary digit. If the letter is in that position, it's a 1. If it's a hyphen, it's a 0. Take a look at Figure \ref{fig:perms_to_binary}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{perms_to_binary}
	\caption{Each character represents either a 1 or a 0}
	\label{fig:perms_to_binary}
\end{figure}

Now we have three binary numbers for each section. Let's start with the first section, \texttt{rw-} or \texttt{110}. The right-most digit is the 1's column. The middle digit is the 2's column, and the leftmost digit is the 4's column. So we have one 4, and one 2. This means that \texttt{rw-} is octal 6. Refer to Figure \ref{fig:bin_to_octal} to see the whole example.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{bin_to_octal}
	\caption{Converting the binary to octal form}
	\label{fig:bin_to_octal}
\end{figure}

So the above permissions string is represented simply as \texttt{664} in octal. You can see why this is much easier for humans to remember and type!
\subsubsection{chmod: Change Mode}
This is our first command for changing the permissions on a file. The octal representation of a permissions string is called the \textit{mode} which is why the name of this command is Change Mode. \texttt{chmod} is a command you will end up using a lot, so taking the time to remember its syntax is time well spent.

In its most basic form, the syntax is simply:

\verb+chmod octal filename+

So to change \texttt{LL.txt} to have read and write permissions for the owner, and no permissions for anyone else, the command would be:

\verb+chmod 600 LL.txt+

Try doing this, and then take a look at the output of \texttt{ls -l}. You'll notice that the output for that file has changed, and now we see this:

\verb+-rw------- 1 ubuntu ubuntu  132 Jun  9 10:14 LL.txt+

There is another way to add and remove modes from a file. \texttt{chmod} can also accept another kind of syntax. You specify which group you want to affect: either \texttt{u} for owner, \texttt{g} for group, \texttt{o} for others, or \texttt{a} for all groups. This letter is then followed by a plus or a minus sign, and then the bit you want to add or remove. These are the letters we are already familiar with: \texttt{r} for read, \texttt{w} for write, and \texttt{x} for execute. So, if I wanted to add the read and write permissions back to \texttt{LL.txt}, I could do it like this:

\verb#chmod g+rw LL.txt#

\subsubsection{chown: Change Owner and chgrp: Change Group}
As we have learned, the only user who can change the attributes of a file (including permissions) is the owner of that file, or the superuser who can override anything. But what if you need to change the owner of a file? Well, the syntax is almost identical to \texttt{chmod} except you put the name of the user instead of the permissions you want to change. For example:

\verb+chown ubuntu LL.txt+

However, you must be the owner of a file to change the owner! That means you can give away ownership, but you can't claim it on your own. We will go over how to do this when we learn about the superuser in the next chapter!

\texttt{chgrp} has the exact same syntax as \texttt{chown}, except of course instead of a username you put the name of a group. However, there is a shortcut for changing both the user and the group at the same time, using \texttt{chown}. The syntax is like this:

\verb+chown user:group filename+

Both \texttt{chown} and \texttt{chgrp} can accept the \texttt{-r} flag to act recursively; that is, change the owner/group for all files and directories within a directory.

\begin{boxedtoc}{Chapter Exercises}
	\begin{enumerate}
		\item What is /?
		\item What directory do system-wide configuration files go in?
		\item Where can I find binaries like \texttt{ls} and \texttt{cat}?
		\item How can I tell if an entry in \texttt{ls -l} is a file or a directory?
		\item What permissions does this give? \texttt{-rwxr-x---}
		\item What permissions does this give? \texttt{640}
		\item Convert \texttt{-r-xr-xr-x} into octal form.
		\item Convert \texttt{440} into long form.
		\item What does the command \texttt{chmod o+x} do?
		\item How can I make all the files in a directory be owned by \texttt{ubuntu}?
		\item How can I make all the files, directories and sub-directories owned by the group \texttt{jimmy}?
		\item What syntax is used to change both the owner and group of a file at the same time? Show an example that changes \texttt{LL.txt} to be owned by \texttt{ubuntu} and in group \texttt{ubuntu}.
	\end{enumerate}
\end{boxedtoc}
