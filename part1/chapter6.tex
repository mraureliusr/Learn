% !TeX root = ../learn_linux_amr.tex
\chapter{Users \& Groups} \label{ch:Users-Groups}
Users and groups were briefly covered in the previous chapter on permissions, but now we'll look at them more in-depth. We'll discuss the \textit{root} or \textit{superuser} who is, in effect, the administrator and most powerful user in the system. The root user has the power to edit other users, override any changes made to the system by other users, and can change any part of the system. This is a lot of power, which is why today it's advised to never actually log in as the root user; rather, we use a tool called \texttt{sudo} to run commands as the superuser without having to log in as the superuser.
\section{Users}
\subsection{Superuser}
The name \textit{superuser} is a particularly apt name. Think of them like a superhero -- they have powers that nobody else has. They can smash through barriers put up by other users; they can bend the system to their will. These powers are fundamental to running a Linux system, but because the superuser is so powerful, you also have to be careful when you use it, and think before you act. The superuser is so powerful, in fact, that they can completely destroy the system and render it unbootable in myriad ways.

The name of the superuser is typically \textit{root}, and as such we use the names interchangably. The root user's home directory (as we saw back in chapter 5) is in the top level of the entire system, and is simply called \texttt{/root}. Only the root user can access files in here. The reason the root user's home directory is in a separate place than the rest of the users is intentional -- if something goes wrong with the system and the /home folder is inaccessible, then the root user will still be able to access their files. This also underlines how important the root user is to the system -- it is so integral to the system that it gets a VIP parking spot for its files!

Back in the early days of Unix, and even through to the 90s, it was common for the owner of a machine to simply use the root account and never create a separate user account for themselves. This gives you full access to the system at all times, which can be very convienient. But as system administrators learned, it also bypasses any safeguards in place to protect the system from accidental damage, and thus being logged in as root means a single errant keystroke can have disastrous consequences. Therefore, as time passed, the recommendation is to never log in as the root user unless absolutely necessary, and to instead temporarily \textit{elevate} your user with superuser powers using a tool like \texttt{sudo}.

\subsection{sudo: Superuser Do}
\texttt{sudo} is a command which, if the user has the correct permissions, will allow you to execute a command as if the root user was executing it. This is necessary for many reasons -- to edit system configuration files, to add and remove packages, to edit, add and remove other users, etc. \texttt{sudo} has a few safeguards in place, but generally you should always stop and think before using it to make sure the command you're entering needs to be run by the superuser, and that the command you are entering is correct and won't have any unforeseen side effects.

The first time you try to use \texttt{sudo}, you will get a bit of a lecture from the operating system:

\begin{verbatim}
	We trust you have received the usual lecture from the local System Administrator.
	It usually boils down to these three things:
	
	#1) Respect the privacy of others.
	
	#2) Think before you type.
	
	#3) With great power comes great responsibility.
\end{verbatim}

Experienced users don't even stop to think about this warning, but we're going to take a careful look at what it means. Being able to run commands as superuser is like handing Ferrari keys to a teenager with a new license. It's extremely powerful, and can be very satisfying, but you can very easily wreck things and damage your system.

Respecting the privacy of others doesn't apply on a single-user system like our example system, but in most multi-user environments it does. The superuser can access \textit{any} file on the computer. This means you can read other peoples' emails, their private text files, etc. System administrators take this power very seriously. Most major Linux installations monitor what regular users are doing with the \texttt{sudo} command, so if you get caught poking around you will lose your account!

Thinking before you type is so important. Remember earlier when I showed you the command to delete every file on the system, without prompting the user to make sure that's what they want to do? That's an extreme example, but you can very easily get into trouble when running commands you don't understand. That is why I will say this again here: \textbf{do not just copy and paste commands from the Internet unless you have taken the time to understand what they are doing to your system}.

The third rule is again not totally applicable to a single-user system, but it's something to keep in mind if you ever use a multi-user system (for example, at a university). If you have superuser permissions, you can make the system amazing, or you can completely destroy it. That power rests on your shoulders, and that's why understanding what you are doing is so important. That's why you are reading this book!

So now that we have that out of the way, we can start to talk about the command itself. \texttt{sudo} is extremely simple: you simply prefix any command you want to run as superuser with \texttt{sudo}. It will prompt you for your user password (which, if you recall, is \textbf{learnlinux}) before allowing you to continue.

On many systems you can only use \texttt{sudo} if you are a member of a particular group (typically called \textit{admin}, \textit{sudo}, or \textit{wheel}). If you don't have permission to use the \textit{sudo} command, then you'll get a message like this:

\begin{verbatim}
	User is not in the sudoers file.  This incident will be reported.
\end{verbatim}

Talk about a scary message to get! Who is it being reported to? The \texttt{sudo} police? What actually happens is an entry gets made in the system log, showing the time and date of the attempt to use \texttt{sudo}, the user who tried to use it, and what command they were trying to run. On single-user systems this doesn't matter, but on large multi-user systems like universities these reports are monitored and you might get an email from the system administrator asking what exactly you were trying to do!

If you run across this message on a system you own, it means that your user is either not in one of the above-mentioned groups, or that group has not been correctly configured inside the \texttt{/etc/sudoers} file. This file configures a bunch of things about \texttt{sudo}, but near the bottom there are often example configurations to enable one of the above-named groups to use \texttt{sudo}.

\begin{boxedsec}{Tip}
If you ever do need to configure the sudoers file, you must first log in as the root user (one of the aforementioned few examples where this is necessary). This can be done by actually logging in as the root user, or, if you know the root user's password, you can use the \texttt{su} (switch users) command:

\verb|su -|

You'll be prompted for the root password, and then a root shell will be launched. Use an editor like \texttt{nano} to edit \texttt{/etc/sudoers}, and find the line that looks like this:
\begin{verbatim}
	## Uncomment to allow members of group wheel to execute any command
	# %wheel ALL=(ALL) ALL
\end{verbatim}
Remove the \# and the space following it at the start of that second line, then save the file. Ensure your user is in the wheel group (refer to the following section on groups), and then they should be able to use \texttt{sudo} with no issues!
\end{boxedsec}

\subsection{Listing Users}
First, let's take a look at the list of the current users on the system. All the information about every user on the system is stored in a file called \texttt{/etc/passwd}. As suggested by the name of this file, passwords used to be stored in this file too (in plain text!) but that has long since changed to much more secure methods. We can simply run \texttt{cat /etc/passwd}. This will give us a huge wall of text, but let's just take a look at a single line:

\verb+ubuntu:x:1000:1000:ubuntu,,,:/home/ubuntu:/bin/bash+

Every piece of information is separated by a colon (:). The first one is the username, clearly. The second one is where the password used to be. It's represented by a lower-case x now. The next two fields are the numerical versions of your user and group. There are known as the \texttt{UID} and \texttt{GID} respectively. Following those is an optional text description of the user; then we have the location of that user's home folder, and finally the shell that they use on login. 

All of these options can be customized when you add a new user. Why don't we create a new user for you?

\subsection{Adding a new user}
The command that is used on almost every Linux system to add users is a command called, surprise, \texttt{useradd}. However, Ubuntu added a script that makes adding users much easier, and that's what we will use. They gave it the creative name of \texttt{adduser}. If you try to run that now, it will tell you that only root can add a user, so run it again with \texttt{sudo}. The syntax is simply:

\verb+adduser <username>+

So let's give it a try. I'm going to add a user named \texttt{jimmy}:

\begin{verbatim}
ubuntu@ubuntu-VirtualBox:~$ sudo adduser jimmy
Adding user `jimmy' ...
Adding new group `jimmy' (1001) ...
Adding new user `jimmy' (1001) with group `jimmy' ...
Creating home directory `/home/jimmy' ...
Copying files from `/etc/skel' ...
Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
Changing the user information for jimmy
Enter the new value, or press ENTER for the default
	Full Name []: Jimmy Smith
	Room Number []: 201
	Work Phone []: 555-0194
	Home Phone []: 555-0195
	Other []: 
Is the information correct? [Y/n] Y
\end{verbatim}

Note that none of the values besides the password is required. You can enter them if you wish, or just hit Return to skip them. That was pretty easy, wasn't it?

\begin{boxedsec}{Note$!$}
	Ubuntu includes simple scripts like \texttt{adduser} to make life a bit easier. Just keep in mind that many other distributions do not have these scripts! Instead, you will have to use the \texttt{useradd}, \texttt{usermod}, \texttt{userdel}, \texttt{groupadd}, and \texttt{groupdel} commands. The syntax is similar, but these commands don't have the interactive mode that the Ubuntu scripts have, so you'll likely need to read the \texttt{man} pages if you run into any issues.
\end{boxedsec}

\subsection{Changing users}
If you want to log in as your newly created user, you've got two ways to do it. If you just want to be them in the terminal, we can use a handy command called \texttt{su}. This is the ancestor of \texttt{sudo}, except it allows you to actually log in as any user. I'm going to log in as \texttt{jimmy}:

\begin{verbatim}
ubuntu@ubuntu-VirtualBox:~$ su jimmy
Password: 
jimmy@ubuntu-VirtualBox:/home/ubuntu$
\end{verbatim}

When it asks you for the password, it's asking for that user's password, not your own. So type in whatever password you just created. To go back to your prompt, just type \texttt{exit}. That was easy!

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{images/loggingout}
	\caption[Logging out]{Navigate to the Logout option in the Power Options menu}
	\label{fig:loggingout}
\end{figure}


If you want to log in as this user in the desktop environment, you can simply log out by clicking the power icon in the top right of the screen and clicking on your username; then choose Logout. See figure \ref{fig:loggingout}.

\subsection{Modifying users}
Perhaps you made a mistake when creating the user, or perhaps you want to allow the user to use \texttt{sudo}. The command for editing users is \texttt{usermod}. There are many parameters that can be changed, but the most frequently used is adding a user to a group. This is achieved with the following command:

\verb+usermod -a -G sudo jimmy+

The \texttt{-a} stands for \textit{append}, meaning we want to add to the existing groups and not replace them. The \texttt{-G} means we want to add or replace the \textit{supplementary} groups this user is in, and not their \textit{primary} group. In this command, we are adding the user \texttt{jimmy} to the \texttt{sudo} group so they can use the \texttt{sudo} command. On other distributions, this group is sometimes called \texttt{wheel}.

\begin{boxedsec}{Note$!$}
	Primary groups are distinct from supplementary groups for a reason. On almost all modern Linux distributions, every user has a primary group that matches their username. For example, \texttt{jimmy}'s primary group would be \texttt{jimmy}. It's important not to change the primary group unless you know what you are doing!

	Also, never change a user's group to \texttt{root}. \texttt{sudo} was written for a good reason, and it's much safer to use \texttt{sudo} than having a user be \texttt{root} all the time.
\end{boxedsec}

\subsection{Deleting users}
If the command to add users was \texttt{adduser}... maybe the command to delete users will be \texttt{deluser}? Let's give it a try:

\begin{verbatim}
ubuntu@ubuntu-VirtualBox:~$ sudo deluser
Enter a user name to remove: jimmy
Removing user `jimmy' ...
Warning: group `jimmy' has no more members.
Done.
\end{verbatim}

Note that it has given us a warnig that group \texttt{jimmy} has no more members. If we wanted to remove the group at the same time, we could pass the \texttt{--group} option to \texttt{deluser}.

\subsection{Deleting groups}
To remove a group we use the \texttt{delgroup} command.

\subsection{Changing passwords} \label{subsec:Changing-passwords}

Changing your password is an important thing to do if you plan to use this Ubuntu installation, or any Linux installation, long term. The command is very simple:

\verb+passwd <new password>+

It will ask you to enter your current password, followed by the new password twice. Remember, the current default password is simply \textbf{learnlinux} and I strongly recommend changing it if you plan to use this copy of Ubuntu for anything beyond just following this course.