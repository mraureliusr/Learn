* Part 1: Starting out with Linux
    * [Chapter 1: Introduction](part1/chapter1.tex)
    * [Chapter 2: Getting Started](part1/chapter2.tex)
    * [Chapter 3: Welcome to Linux!](part1/chapter3.tex)
    * [Chapter 4: Editors](part1/chapter4.tex)
    * [Chapter 5: Files and permissions](part1/chapter5.tex)
    * [Chapter 6: Installing Software](part1/chapter6.tex)
    * [Chapter 7: Users](part1/chapter7.tex)
      * Editing sudoers?
* Part 2: Diving Deeper
    * SSH?
    * Chapter X: Intermediate Commands? (PATH as an infobox)
    * Chapter X: Shell Scripting
    * Chapter X: Customising Budgie?
    *
    * Chapter X: Devices (mounting and formatting drives, etc)
* Part 3: Programming in Linux Pt. 1  
Cover gcc, make, gdb (in emacs?), CodeBlocks?
    * Chapter X: The C Programming Language
* Part 4: Programming in Linux Pt. 2  
Testing?
    * Chapter X: The Python Scripting Language
    * Chapter X: Using Python for simple tasks

* Appendix:
    * Source control with Git
    * Building software from source
    * Environment variables
    * Regular Expressions
    * Customising BASH? (command aliases?)
    * Using WINE

* Answers to Chapter Exercises

Web server? WordPress/LAMP stack?

===============NOTES================

Intermediate Commands:

* man (explain man pages)
* ifconfig/iwconfig?
*
* wget
* rsync?
